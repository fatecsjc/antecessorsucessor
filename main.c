#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");
    printf("Hello mund�o!\n");

    int n;
    printf("Informe um n�mero qualquer do tipo inteiro: ");
    scanf("%i", &n);

    printf("\nO n�mero antecessor do n�mero informado �: %i", n - 1);
    printf("\nO n�mero sucessor do n�mero informado �: %i", n + 1);
    return 0;
}
